# Trivia game

## Description

Trivia game is an application where users can register an account and select a series of questions to challenge themselves.

The game can be found here: [Trivia Game](https://murmuring-ridge-71190.herokuapp.com/)


The application has 3 parts:

### Home
In Home the user can select a username, the amount of questions wanted (maximum 50), question category, difficulty and type of questions (multiple or true/false). Users can also choose to not select category, difficulty or type, to get randomized questions. 

Some of the categories do not have 50 questions in total so you might experience not always getting the amount you wanted.

### Trivia
In Trivia users get shown the questions based on previously selected parameters in home. Only one question is shown at a time and it changes the amount of buttons based on the question type.
When all questions are answered the user will be routed to the result screen

### Result
In Result the user is shown the previously answered questions. The true answer will be shown along with the answer given by the user and a score. If the selected user has played before their stored score will only be updated if they achive a new high score.

There are two buttons, "Back to home" which will take you back to the homescreen where the user can choose new parameters to base the quiz from, and "Retry" which will give you new questions based on the previously given parameters. 

## Installation

Write the commands bellow into the terminal for installation of used modules

```bash
npm install
npm install he
npm install -D tailwindcss
```

## Usage
```bash
npm run dev
```
# API


### User API
The user API stores the user information in JSON format. The API stores username and a highscore.

### Trivia API
Trivia API used by application [Trivia](https://opentdb.com/api_config.php). Here we get different questions based on what type of category, type and difficulty is selected. The number of questions wanted can also be specified. Based on a session genereted token, a user will never get the same questions during a playthrough.

## Contributing
[Karianne Tesaker](https://gitlab.com/kariannet) and [Erlend Hollund](https://gitlab.com/Holdude)

## Component tree

![ComponetTree.png](https://gitlab.com/kariannet/assignment-2-trivia-game/-/raw/main/figures/componentTree.png)
