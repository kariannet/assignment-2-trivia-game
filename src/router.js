import { createRouter, createWebHistory} from "vue-router";
import Home from "./views/Home.vue";
import Result from "./views/Result.vue";
import Trivia from "./views/Trivia.vue";

const routes = [
    {
        path: "/",
        component: Home
    },
    {
        path: "/Result",
        component: Result
    },
    {
        path: "/Trivia",
        component: Trivia
    }
];

export default createRouter({
    history: createWebHistory(),
    routes
})