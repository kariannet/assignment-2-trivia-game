import { createStore } from "vuex";
import { apiFetchQuestionCategories } from "./api/questionCategories";
import { apiLoginOrRegister, apiUpdateUserHighScore } from "./api/users"
import { apiFetchQuestions } from "./api/questions"
import { apiFetchToken } from "./api/sessionToken";

/**
 * Checks if input value is stored in localStorage.
 * @param {string} valueName - value to check for
 * @param {boolean} isObject - true if expected value is an object
 * @returns the stored value if it exists.
 */
const init = (valueName, isObject=false) => {
    const storedValue = localStorage.getItem(valueName);
    if (!storedValue) {
        return "";
    }
    if (isObject) {
        return JSON.parse(storedValue);
    }
    return storedValue;
}

export default createStore({
    state: {
        user: init("trivia-user", true),
        sessionToken: init("sessionToken"),
        questions: [],
        categories: [],
        indexQuestion: 0,
        chosenCategory: init("trivia-category"),
        difficulty: init("trivia-difficulty"),
        amountQuestion: init("trivia-numQuestions"),
        questionType: init("trivia-type"),
        loading: true,
        score: 0,
        errorFetchQuestions: false

    },
    getters: {
    },
    mutations: {
        setUser: (state, user) => {
            state.user = user;
        },
        setChosenCategory: (state, category) => {
            state.chosenCategory = category;
        },
        setDifficulty: (state, difficulty) => {
            state.difficulty = difficulty;
        },
        setAmountQuestion: (state, amountQuestion) => {
            state.amountQuestion = amountQuestion;
        },
        setCategories: (state, categories) => {
            state.categories = categories;
        },
        setLoading: (state, loading) => {
            state.loading = loading
        },
        setQuestions: (state, questions) => {
            state.questions = questions;
        },
        setQuestionType: (state, questionType) => {
            state.questionType = questionType;
        },
        setIndex:(state, indexQuestion) =>{
            state.indexQuestion = indexQuestion
        },
        setUserAnswers:(state, userAnswers) =>{
            state.userAnswers = userAnswers
        },
        // updates element in array based on current index
        setThisQuestion:(state, thisQuestion) =>{
            state.questions[state.indexQuestion] = thisQuestion
        },
        setSessionToken:(state, sessionToken)=>{
            state.sessionToken = sessionToken
        },
        setScore:(state, score)=>{
            state.score = score
        },
        setErrorFetchQuestions:(state, errorFetchQuestions) => {
            state.errorFetchQuestions = errorFetchQuestions;
        }


    },

    actions: {
        // Fetches questions from api, gives parameters from store
        async fetchQuestions({commit}){
            try {
                commit("setErrorFetchQuestions", false);
                // Set the var loading to true while waitig for data
                commit("setLoading", true)
                // API request
                const [error, questions] = await apiFetchQuestions(this.state.amountQuestion, this.state.chosenCategory, this.state.difficulty, this.state.questionType, this.state.sessionToken)
            
                if(error !== null){
                    throw new Error(error);
                }
                if (questions.response_code > 0) {
                   commit("setErrorFetchQuestions", true);
                }
                // Set the fetched questions
                commit("setQuestions", questions.results)
                // Fetching complete, setting loading
                commit("setLoading", false)
                return null // error 

            }
            catch (error) {
                return error;
            }
        },

        // action for calculating score
        // loop for every object in questions
        calculateScore(){
            // holder for totalt score
            let scoreCount = 0
            const arrayQ = this.state.questions
            arrayQ.forEach(element => {
                scoreCount += element.score
            });
            // commiting score
            this.commit("setScore", scoreCount)

        },
        
        // Gets session token from triva API
        async fetchSessionToken( {commit} ){

            const [error, token] = await apiFetchToken()

            if(error !== null){
                return error
            }


            // Set session token to store
            commit("setSessionToken", token.token)
            // set to storage
            localStorage.setItem("sessionToken", token.token);

            return null
        },
        
        // Adds the users answer and score to the questions object
        changeUserAnswers (context, answer) {
            // gets current question obj
            let currentQ = this.state.questions[this.state.indexQuestion];
            currentQ.userAnswer = answer.svar
            
            // If user guessed correct give points
            if(currentQ.correct_answer == answer.svar){
                currentQ.score = 10
            } else {
                currentQ.score = 0
            }
            // Puts updated question object back in array
            this.commit("setThisQuestion", currentQ)  

            currentQ = this.state.questions[this.state.indexQuestion]
            
        },

        // actions to be performed at clicking the start button on the start screen.
        async start( { commit }, { 
            username, 
            numQuestions, 
            selectCategory, 
            selectDifficulty, 
            selectType 
        }) {
            try {
                if (!username.value) {
                    throw new Error("You forgot to fill in your username.");
                }

                if (!numQuestions.value) {
                    throw new Error("Must fill in number of questions.")
                }

                const [error, user] = await apiLoginOrRegister(username.value);

                if (error !== null) {
                    throw new Error(error);
                }

                commit("setUser", user);
                localStorage.setItem("trivia-user", JSON.stringify(user));

                commit("setChosenCategory", selectCategory.value);
                localStorage.setItem("trivia-category", selectCategory.value);

                commit("setDifficulty", selectDifficulty.value);
                localStorage.setItem("trivia-difficulty", selectDifficulty.value);

                commit("setAmountQuestion", numQuestions.value);
                localStorage.setItem("trivia-numQuestions", numQuestions.value);
                
                commit("setQuestionType", selectType.value);
                localStorage.setItem("trivia-type", selectType.value);

                return null;

            }
            catch(e) {
                return e.message;
            }
        },

        // fetching question categories
        async fetchAllCategories({ commit }) {
            // API request
            const [ error, categories ] = await apiFetchQuestionCategories();

            if (error !== null) {
                return error;
            }

            commit("setCategories", categories);

            return null;
        },

        // updates the user high score in database
        async updateUserHighScore({ commit }, {userID, highScore}) {
            // API request
            const [ error, currentUser ] = await apiUpdateUserHighScore(userID, highScore);

            if (error != null) {
                return error;
            }

            commit("setUser", currentUser);
            localStorage.setItem("trivia-user", JSON.stringify(currentUser));

            return null;

        }
    },
});