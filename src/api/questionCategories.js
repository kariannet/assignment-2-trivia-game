import { useStore } from "vuex";

/**
 * Fetches the question categories from the API.
 * @returns An array with the possible error and the result from the API.
 */
export async function apiFetchQuestionCategories(){
    try {
        const response = await fetch("https://opentdb.com/api_category.php");

        if (!response.ok) {
            throw new Error("Could not find question categories.");
        }

        const data = await response.json();

        return [ null, data.trivia_categories ]
    }
    catch(error){
        return [ error.message, [] ]
    }
}
