import { useStore } from "vuex"
import {QUESTION_BASE_URL} from "."

export async function apiFetchQuestions(amountQuestion, chosenCategory, difficulty, questionType, sessionToken){
    try {
        // Fetches the questions based on the input from the user
        const response = await fetch(`${QUESTION_BASE_URL}amount=${amountQuestion}&category=${chosenCategory}&difficulty=${difficulty}&type=${questionType}&token=${sessionToken}`);
        const resultat = await response.json()
        

        return [null, resultat]
    }
    catch(error){
        console.log(error)
        return [error.message, []]
    }
}