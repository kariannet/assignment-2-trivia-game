import { USER_BASE_URL, API_KEY } from "./";

/**
 * Fetches user from API based on username, or registers the user if
 * it does not exist.
 * @param {string} username 
 * @returns Array with potential error and the response data (user)
 */
export async function apiLoginOrRegister(username) {
    try {
        // Send GET-request for user
        const response = await fetch(`${USER_BASE_URL}/trivia?username=${username}`);
        const data = await response.json();

        // Check if user existed, if not, register user:
        if (!data[0]) {
            const config = {
                method: "POST",
                headers: {
                    "X-API-KEY": API_KEY,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    username: username,
                    highScore: 0
                })
            };
            const newResponse = await fetch(`${USER_BASE_URL}/trivia`, config);
            if (!newResponse.ok) {
                throw new Error("Could not create new user.");
            }

            const newData = await newResponse.json();

            return [ null, newData ];
        }

        // If user already existed, return user-data, 
        // if more users are saved with same username, only the first is returned
        return [ null, data[0] ];

    }

    catch (error) {
        return [ error.message, null ];
    }
};

/**
 * Updates the user high score in the API.
 * @param {string} userID 
 * @param {number} score 
 * @returns Array with potential error and response data (user)
 */
export async function apiUpdateUserHighScore(userID, score) {
    try {
        const config = {
            method: "PATCH",
            headers: {
                "X-API-KEY": API_KEY,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                highScore: score
            })
        };

        const response = await fetch(`${USER_BASE_URL}/trivia/${userID}`, config);
        
        if (!response.ok) {
            throw new Error("Could not update high score.");
        };

        const data = await response.json();

        return [ null, data ]; 
    }
    catch(error) {
        return [ error.message, null ];
    }
};

