// Fetching session token
export async function apiFetchToken(){
    try{
        const response = await fetch("https://opentdb.com/api_token.php?command=request")
        const resultat = await response.json()

        return [null, resultat]
    }
    catch(error){
        return[error.message,[]]
    }

}